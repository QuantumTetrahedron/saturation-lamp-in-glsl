#ifndef PLANE_H
#define PLANE_H
#include "Object.h"

class Plane : public Object{
public:
    Plane(glm::vec3 col, glm::vec3 pos, glm::vec3 rot = glm::vec3(0.0f), glm::vec3 sc = glm::vec3(1.0f)) : Object(col,pos,rot,sc){}
    std::string meshType() const {return "plane";}
};

#endif // PLANE_H
