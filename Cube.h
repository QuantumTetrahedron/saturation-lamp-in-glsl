#ifndef CUBE_H
#define CUBE_H
#include "Object.h"

class Cube : public Object{
public:
    Cube(glm::vec3 col, glm::vec3 pos, glm::vec3 rot = glm::vec3(0.0f), glm::vec3 sc = glm::vec3(1.0f)) : Object(col,pos,rot,sc){}
    std::string meshType() const {return "cube";}
};


#endif // CUBE_H
