#version 330 core

// this shader allows modifying saturation without the need to pass HSV values.
// All calculations are done on the RGB color format.
// To see how to use the HSV format refer to "HSV.frag"

in vec3 Normal; 
in vec3 FragPos;

// a normal lamp position
uniform vec3 lightPos;

// a saturating lamp position
uniform vec3 saturatorPos; 

// camera position
uniform vec3 viewPos;

// color (should be passed in RGB)
uniform vec3 color;

out vec4 out_color;

// simple Blinn-Phong lightning used for both lamps
float BlinnPhong(vec3 lampPos){
	float ambient = 0.1;
	
	vec3 norm = normalize(Normal);
	vec3 lightDir = normalize(lampPos - FragPos);
	float diffuse = max(dot(norm,lightDir), 0.0);

	vec3 viewDir = normalize(viewPos - FragPos);
	vec3 halfwayDir = normalize(lightDir + viewDir);
	float specular = pow(max(dot(norm, halfwayDir), 0.0), 32) * 0.5;

	return ambient+diffuse+specular;
}

void main(){
	
	// calculating the value of light
	float V = BlinnPhong(lightPos);

	// calculating the saturation, done in the same way, as
	// a saturation lamp gives color in the same way a normal lamp gives light
	float S = BlinnPhong(saturatorPos);

	// calculating the new RGB value:
	// * The light shading scales the rgb components down as the light is in <0,1> range
	// 	 The multiplication is a scaling function with a fixed point in 0.
	// 	 This means that for a Value of 0 the colors all scale to 0 leaving a black area.
	//
	// * Applying the same to saturation - a saturation of 0 means that the RGB components
	//   scale to V as totally desaturated objects are in grayscale:
	
	//          white (V=1,S=0) *-----* (V=1, S=1) colored
	//                          |    /
	//                          |   /
	//         gray (V=0.5,S=0) *--* (V=0.5,S=1) colored but darker
	//                          | /
	//                          |/
	//   black (V=0,S=anything) *
	
	// 	 As seen above, going from V=0.5,S=1 to V=0.5,S=0 
	// does not return the color white ((1,1,1) in RGB) but gray (actually (V,V,V) in RGB)

	// * This means that before applying the scaling function from regular light
	//	 a different scaling function should be used to apply saturation
	//   and this new function must have the fixed point in V

	// * The default function with fixed point 0 is f(x) = V * x where V belongs to <0,inf)
	//
	// * The scaling function with a fixed point in Vo is g(x) = (Vo - S(Vo - x))
	//   where S belongs to <0,inf)
	//
	// * A problem appears as g(x) is using the Value component Vo of the HSV model
	//   which we do not know (The V mentioned earlier is just the value given
	//   by the lamp but a color might already have one encoded in the initial RGB values).
	//   To solve this the V value can be easily calculated from the original color
	//   by extracting the highest RGB component.
	//
	// * A combination of both of the scaling functions is f(g(x))
	//   which is the same as g(f(x))
	//
	//		f(g(x)) = V * (Vo - S(Vo - x)) where Vo = max(x.r, x.g, x.b)
	//		
	//		g(f(x)) = (Vo - S(Vo - V*x)) where Vo = max(V*x.r, V*x.g, V*x.b)
	//
	// * I've chosen the f(g(x)) approach which can be seen in practice here:

	float OriginalV = max(color.r, max(color.g, color.b));
	vec3 result = V*( vec3(OriginalV) - S * ( vec3(OriginalV) - color ) );

	out_color = vec4(result, 1.0);
}