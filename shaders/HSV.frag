#version 330 core

// this shader allows modifying saturation by passing the color in HSV format,
// editing the saturation component and converting to RGB before outputting.
// Consult "noHSV.frag" to see how to do all the calculations in RGB. 

in vec3 Normal; 
in vec3 FragPos;

// a normal lamp position
uniform vec3 lightPos;

// a saturating lamp position
uniform vec3 saturatorPos; 

// camera position
uniform vec3 viewPos;

// color (should be passed in HSV)
uniform vec3 color;

out vec4 out_color;

// a function for converting from HSV to RGB
vec3 hsvToRgb(vec3 hsv)
{
	float S = hsv.g;
	float V = hsv.b;
	if(S > 1.0) S = 1.0;
	if(V > 1.0) V = 1.0;

	float C = V * S;
	float X = C * (1 - abs(mod(hsv.r*6, 2)-1));
	float m = V - C;

	vec3 RGB;
	if(hsv.r < 1.0/6.0){
		RGB = vec3(C,X,0);
	}
	else if(hsv.r < 2.0/6.0){
		RGB = vec3(X,C,0);
	}
	else if(hsv.r < 3.0/6.0){
		RGB = vec3(0,C,X);
	}
	else if(hsv.r < 4.0/6.0){
		RGB = vec3(0,X,C);
	}
	else if(hsv.r < 5.0/6.0){
		RGB = vec3(X,0,C);
	}
	else{
		RGB = vec3(C,0,X);
	}

	vec3 rgb = RGB + vec3(m);

	return rgb;
}

// simple Blinn-Phong lightning used for both lamps
float BlinnPhong(vec3 lampPos){
	float ambient = 0.1;
	
	vec3 norm = normalize(Normal);
	vec3 lightDir = normalize(lampPos - FragPos);
	float diffuse = max(dot(norm,lightDir), 0.0);

	vec3 viewDir = normalize(viewPos - FragPos);
	vec3 halfwayDir = normalize(lightDir + viewDir);
	float specular = pow(max(dot(norm, halfwayDir), 0.0), 32) * 0.5;

	return ambient+diffuse+specular;
}

void main()
{
    float hue = color.r;

    // The saturator lamp gives color in the same way a normal lamp gives light
    // so the same calculations are used.
    float sat = color.g * BlinnPhong(saturatorPos);
    
    // Default lightning affects the Value component of the HSV color
    // Given a ToRGB function which converts HSV to RGB and any number N:
    // 		( [H] )				(  [H]  )
    // ToRGB( [S] ) * N == ToRGB(  [S]  )
    //		( [V] )				( [V*N] )
    // Therefore the result is the same as with the usual lightning calculations.
    float val = color.b * BlinnPhong(lightPos);

    // lastly the color converted to RGB is returned
    out_color = vec4(hsvToRgb(vec3(hue, sat, val)),1.0f);
}