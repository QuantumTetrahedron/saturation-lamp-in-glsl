#ifndef OBJECT_H
#define OBJECT_H

class Object{
public:

    glm::vec3 position;
    glm::vec3 rotation;
    glm::vec3 scale;
    glm::vec3 color;

    glm::mat4 GetModelMatrix() const{
        glm::mat4 m;
        m = glm::translate(m, position);
        if(glm::length(rotation)>0.0f)
        {
            m = glm::rotate(m, glm::radians(rotation.y), glm::vec3(0.0f,1.0f,0.0f));
            m = glm::rotate(m, glm::radians(rotation.z), glm::vec3(0.0f,0.0f,1.0f));
            m = glm::rotate(m, glm::radians(rotation.x), glm::vec3(1.0f,0.0f,0.0f));
        }
        m = glm::scale(m, scale);
        return m;
    }
    virtual std::string meshType() const { return " - ";}
protected:

    Object(glm::vec3 col, glm::vec3 pos, glm::vec3 rot = glm::vec3(0.0f), glm::vec3 sc = glm::vec3(1.0f))
    :position(pos), rotation(rot), scale(sc), color(col){}
};

#endif // OBJECT_H
