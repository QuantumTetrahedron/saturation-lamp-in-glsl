#ifndef LAMP_H
#define LAMP_H

class Lamp : public Cube{
public:
    Lamp(glm::vec3 position) : Cube(glm::vec3(1.0f,1.0f,1.0f),position,glm::vec3(0.0f),glm::vec3(0.2f)){}

    virtual void UpdateShader(const Shader& shader) const {
        shader.use();
        shader.setVec3("lightPos", position);
    }
};

#endif // LAMP_H
