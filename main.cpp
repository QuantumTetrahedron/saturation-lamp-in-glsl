#include <string>

#define GLEW_STATIC
#include <GL/glew.h>

#include <GLFW/glfw3.h>

#include "Shader.h"
#include "Camera.h"
#include "Renderer.h"
#include "Saturator.h"

GLuint screenWidth = 800, screenHeight = 600;

void key_callback(GLFWwindow* window, int key, int scancode, int action, int mode);
void scroll_callback(GLFWwindow* window, double xoffset, double yoffset);
void mouse_callback(GLFWwindow* window, double xpos, double ypos);
void Do_Movement();
void UpdateView(const Shader& shader);

Camera camera(glm::vec3(0.0f, 0.5f, 5.0f));
bool keys[1024];
GLfloat lastX = 400, lastY = 300;
bool firstMouse = true;

GLfloat deltaTime = 0.0f;
GLfloat lastFrame = 0.0f;

int main()
{
    glfwInit();
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);

    GLFWwindow* window = glfwCreateWindow(screenWidth, screenHeight, "LearnOpenGL", nullptr, nullptr);
    glfwMakeContextCurrent(window);

    glfwSetKeyCallback(window, key_callback);
    glfwSetCursorPosCallback(window, mouse_callback);
    glfwSetScrollCallback(window, scroll_callback);

    glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

    glewExperimental = GL_TRUE;
    glewInit();

    glViewport(0, 0, screenWidth, screenHeight);

    glEnable(GL_DEPTH_TEST);
    glEnable(GL_CULL_FACE);

    Shader HSVshader("shaders/default.vert", "shaders/HSV.frag");
    Shader noHSVshader("shaders/default.vert", "shaders/noHSV.frag");
    Shader LampShader("shaders/lamp.vert", "shaders/lamp.frag");

    Cube cube1(glm::vec3(1.0f/4.0f,1.0f,1.0f), glm::vec3(-2.0f,0.0f,0.0f));
    Cube cube2(glm::vec3(0.5f,0.0f,1.0f), glm::vec3(2.0f,0.0f,0.0f));

    /// Two planes with the same color are being drawn on the scene next to each other to show that the shaders output the same
                    ///color in HSV
    Plane plane1(glm::vec3(0.0f, 1.0f, 0.25f), glm::vec3(-5.0f,-0.5f,0.0f), glm::vec3(0.0f), glm::vec3(10.0f, 20.0f, 20.0f));
                    ///color in RGB
    Plane plane2(glm::vec3(0.25f, 0.0f, 0.0f), glm::vec3(5.0f,-0.5f,0.0f), glm::vec3(0.0f), glm::vec3(10.0f, 20.0f, 20.0f));

    Renderer renderer;

    glm::mat4 projection = glm::perspective(camera.Zoom, (float)screenWidth/(float)screenHeight, 0.1f, 100.0f);

    Lamp lamp(glm::vec3(0.0f,1.5f,2.0f));
    lamp.UpdateShader(HSVshader);
    lamp.UpdateShader(noHSVshader);
    Saturator sat(glm::vec3(0.0f,1.5f,-2.0f));
    sat.UpdateShader(HSVshader);
    sat.UpdateShader(noHSVshader);

    HSVshader.use();
    HSVshader.setMat4("projection", projection);
    noHSVshader.use();
    noHSVshader.setMat4("projection", projection);
    LampShader.use();
    LampShader.setMat4("projection", projection);

    glClearColor(0.05f, 0.05f, 0.05f, 1.0f);
    while(!glfwWindowShouldClose(window))
    {
        GLfloat currentFrame = glfwGetTime();
        deltaTime = currentFrame - lastFrame;
        lastFrame = currentFrame;

        glfwPollEvents();
        Do_Movement();

        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        UpdateView(HSVshader);
        UpdateView(noHSVshader);
        UpdateView(LampShader);

        renderer.Draw(cube1, HSVshader);
        renderer.Draw(cube2, noHSVshader);
        renderer.Draw(plane1, HSVshader);
        renderer.Draw(plane2, noHSVshader);
        renderer.Draw(lamp, LampShader);
        renderer.Draw(sat, LampShader);

        glfwSwapBuffers(window);
    }

    glfwTerminate();
    return 0;
}

void UpdateView(const Shader& shader){
    glm::mat4 view = camera.GetViewMatrix();
    shader.use();
    shader.setMat4("view", view);
    shader.setVec3("viewPos", camera.Position);
}

void Do_Movement()
{
    if(keys[GLFW_KEY_W])
        camera.ProcessKeyboard(FORWARD, deltaTime);
    if(keys[GLFW_KEY_S])
        camera.ProcessKeyboard(BACKWARD, deltaTime);
    if(keys[GLFW_KEY_A])
        camera.ProcessKeyboard(LEFT, deltaTime);
    if(keys[GLFW_KEY_D])
        camera.ProcessKeyboard(RIGHT, deltaTime);
}

void key_callback(GLFWwindow* window, int key, int scancode, int action, int mode)
{
    if(key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
        glfwSetWindowShouldClose(window, GL_TRUE);

    if(action == GLFW_PRESS)
        keys[key] = true;
    else if(action == GLFW_RELEASE)
        keys[key] = false;
}

void mouse_callback(GLFWwindow* window, double xpos, double ypos)
{
    if(firstMouse)
    {
        lastX = xpos;
        lastY = ypos;
        firstMouse = false;
    }

    GLfloat xoffset = xpos - lastX;
    GLfloat yoffset = lastY - ypos;

    lastX = xpos;
    lastY = ypos;

    camera.ProcessMouseMovement(xoffset, yoffset);
}

void scroll_callback(GLFWwindow* window, double xoffset, double yoffset)
{
    camera.ProcessMouseScroll(yoffset);
}
