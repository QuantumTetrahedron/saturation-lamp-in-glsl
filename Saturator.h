#ifndef SATURATOR_H
#define SATURATOR_H
#include "Lamp.h"

class Saturator : public Lamp{
public:
    Saturator(glm::vec3 position) : Lamp(position){
        color = glm::vec3(1.0f, 0.5f, 0.0f);
    }

    void UpdateShader(const Shader& shader) const {
        shader.use();
        shader.setVec3("saturatorPos", position);
    }
};

#endif // SATURATOR_H
